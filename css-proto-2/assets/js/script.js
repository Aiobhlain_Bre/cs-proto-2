//Varibles
//
//testing git control on laptop
console.log("start");
let date = new Date();
let hour = date.getHours();
let min = date.getMinutes();
let sec = date.getSeconds();

if(hour == 0)
{
    hour = 0.9;
}
if(min == 0)
{
    min = 0.9;
}
if(sec == 0)
{
    sec = 0.9;
}

let canvasHour = document.getElementById('canvas-hour');
let ctxHour = canvasHour.getContext('2d');
//canvasHour.width 

function drawHour(delta) {
    requestAnimationFrame(drawHour);
    canvasHour.width = canvasHour.width;
    ctxHour.strokeWidth = 100;
   // ctxHour.strokeStyle = "rgba(0, 0, 0, 0.4)";

    ctxHour.fillStyle = "rgba(0,0,255, 0.4)";
    
   // console.log("canvas 1 hello")
    
    let randomLeft = Math.abs(Math.pow( Math.sin(delta/100000), 2 )) * (50);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/100000) + 10), 2 )) * (50);
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/100000)+2), 2 )) * (50);
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/100000)+1), 2)) * (50);
    
    ctxHour.beginPath();
    ctxHour.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxHour.bezierCurveTo(canvasHour.width / 3, randomLeftConstraint, canvasHour.width / 3 * 2, randomRightConstraint, canvasHour.width, randomRight);

    ctxHour.lineTo(canvasHour.width , canvasHour.height);
    ctxHour.lineTo(0, canvasHour.height);
    ctxHour.lineTo(0, randomLeft);


    
    ctxHour.closePath();
    //ctxHour.stroke();
    ctxHour.fill();
   // console.log("canvas 1");
    //ctx.fill();
}
requestAnimationFrame(drawHour)



let canvasMin = document.getElementById('canvas-min');
let ctxMin = canvasMin.getContext('2d');

function drawMin(delta) {
    requestAnimationFrame(drawMin);
    canvasMin.width = canvasMin.width;
    ctxMin.lineWidth = 4;
    //ctxMin.fillStyle = "rgba(0, 0, 0, 0.4)";

    ctxMin.fillStyle = "rgba(0,255,0, 0.4)";
    
    
    
    let randomLeft = Math.abs(Math.pow( Math.sin(delta/10000), 2 )) * 100;
    let randomRight = Math.abs(Math.pow( Math.sin((delta/10000) + 10), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/10000)+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/10000)+1), 2)) * 100;
    
    ctxMin.beginPath();
    ctxMin.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxMin.bezierCurveTo(0, randomLeftConstraint, canvasMin.width / 3 * 2, randomRightConstraint, canvasMin.width, randomRight);

    ctxMin.lineTo(canvasMin.width , canvasMin.height);
    ctxMin.lineTo(0, canvasMin.height);
    ctxMin.lineTo(0, randomLeft);


    ctxMin.closePath();
    //ctxMin.stroke();
    ctxMin.fill();
}
requestAnimationFrame(drawMin)


function drawSec(delta) {
  requestAnimationFrame(drawSec);
    
    canvasSec.width = canvasSec.width;
    ctxSec.lineWidth = 10
    //ctxSec.fillStyle = "rgba(0, 0, 0, 0.4)";

    ctxSec.fillStyle = "rgba(255,0,0, 0.4)";

    switch(hour){
      case 21:
        ctxSec.fillStyle = "rgba(188, 128, 1)";
        ctxSec.strokeStyle = "rgba(188, 128, 1)";
        
    }
    
    
    let randomLeft = (Math.abs(Math.pow( Math.sin(delta/1000), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/1000) + 10), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/1000)+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/1000)+1), 2)) * 100;
    
    ctxSec.beginPath();
    ctxSec.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxSec.bezierCurveTo(canvasSec.width / 3, randomLeftConstraint, canvasSec.width / 3 * 2, randomRightConstraint, canvasSec.width, randomRight);
    ctxSec.lineTo(canvasSec.width , canvasSec.height);
    ctxSec.lineTo(0, canvasSec.height);
    ctxSec.lineTo(0, randomLeft);


    //ctxSec.stroke();
    ctxSec.closePath();
    ctxSec.fill();
    //console.log("canvas 3")
    //ctx.fill();
}
requestAnimationFrame(drawSec);

let canvasSec = document.getElementById('canvas-sec');
let ctxSec = canvasSec.getContext('2d');

var humidity;
var weatherIcon;
var pressure;
var uvIndex;
var temperature;
var temperatureIcon
var windBearing;
var windSpeed;
var weatherSummary;

window.onload = function() {
  humidity = document.getElementById("current-humidity");
  weatherIcon = document.getElementById("current-icon");
  pressure = document.getElementById("current-pressure");
  uvIndex = document.getElementById("current-uvIndex");
  temperature = document.getElementById("current-temperature");
  temperatureIcon = document.getElementById("temperature-icon");
  windBearing = document.getElementById("current-wind-bearing");
  windSpeed = document.getElementById("current-wind-speed");
  weatherSummary = document.getElementById("weather-summary");
}

/*function farenheitToCelsius(k) {
  return Math.round((k - 32) * 0.5556 );
}

function humidityPercentage(h) {
  return Math.round(h * 100);
}

function degreesToDirection(degrees) {
    var range = 360/16;
    var low = 360 - range/2;
    var high = (low + range) % 360;
    var angles = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
    for (i in angles) {

        if(degrees>= low && degrees < high)
            return angles[i];

        low = (low + range) % 360;
        high = (high + range) % 360;
    }
}

function knotsToKilometres(knot) {
  return Math.round( knot * 1.852);
} */

var weatherImages = {
  "clear-day": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Sun_icon.svg/252px-Sun_icon.svg.png",
  "clear-night": "https://cdn.dribbble.com/users/251177/screenshots/3178715/dribbble.gif",
  "rain": "https://cdn3.iconfinder.com/data/icons/weather-16/256/Rainy_Day-512.png",
  "snow": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Nuvola_weather_snow.svg/1000px-Nuvola_weather_snow.svg.png",
  "sleet": "http://www.clker.com/cliparts/f/6/7/4/1206565674431593790Anonymous_simple_weather_symbols_10.svg.hi.png",
  "wind": "http://www.haotu.net/up/4233/128/216-wind.png",
  "fog": "http://www.iconninja.com/files/81/344/943/fog-cloud-hiding-the-sun-weather-interface-symbol-icon.svg",
  "cloudy": "http://camera.thietbianninh.com/images/icon-2.png",
  "partly-cloudy-day": "http://meteo.cw/images_www/weather_icons1/weather_icon_03.png",
  "partly-cloudy-night": "http://icon-park.com/imagefiles/simple_weather_icons_cloudy_night.png",
  "hail": "http://icons.iconarchive.com/icons/icons8/ios7/256/Weather-Hail-icon.png",
  "thunderstorm": "http://findicons.com/files/icons/2613/android_weather_extended/480/thunderstorms.png",
  "tornado": "http://hddfhm.com/images/clipart-of-a-tornado-11.png"
}

var getWeather = function() {
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(function(position){
        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        showWeather(lat, long)
      })
    }
       else {
            window.alert("Could not get location");
      }
  }
 
  function showWeather(lat, long) {
    var url = `https://api.darksky.net/forecast/f672ff13193bfcc40427a678ebfdbc71/${lat},${long}` + `?format=jsonp&callback=displayWeather`;
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
    displayWeather(object)   
  }

var object;

 function displayWeather(object) {
   // humidity.innerHTML = "Humidity: " + humidityPercentage(object.currently.humidity) + "%";
    weatherIcon.src = weatherImages[object.currently.icon];
    /*pressure.innerHTML = "Pressure: " + object.currently.pressure + " mb";
    uvIndex.innerHTML = "uvIndex: " + object.currently.uvIndex;
    temperature.innerHTML = farenheitToCelsius(object.currently.temperature) + " C" + " / " + object.currently.temperature + " F";
   temperatureIcon.src = "https://cdn4.iconfinder.com/data/icons/medicons-2/512/thermometer-512.png";
    windBearing.innerHTML = "Wind Direction: " + degreesToDirection(object.currently.windBearing);
    windSpeed.innerHTML = "Wind Speed: " + knotsToKilometres(object.currently.windSpeed) + " km/h";
    weatherSummary.innerHTML = "Current Location: " + object.timezone + " <br/> <br/> Weather Summary: " + object.currently.summary;
     document.getElementById("current-icon").style.backgroundColor = "hsl(216, 100%, 60%)"; 
    document.getElementById("weather-summary").style.backgroundColor = "hsl(216, 100%, 60%)"; 
    console.log(object);*/
 }









//Weather API

/*const form = document.querySelector(".top-banner form");
const input = document.querySelector(".top-banner input");
const msg = document.querySelector(".top-banner .msg");
const list = document.querySelector(".ajax-section .cities");
const apiKey = "8b46f89ed372cee44f1cc4583a87ac8d";

form.addEventListener("submit", e => {
  e.preventDefault();
  let inputVal = input.value;

  //check if there's already a city
  const listItems = list.querySelectorAll(".ajax-section .city");
  const listItemsArray = Array.from(listItems);

  if (listItemsArray.length > 0) {
    const filteredArray = listItemsArray.filter(el => {
      let content = "";
      //athens,gr
      if (inputVal.includes(",")) {
        //athens,grrrrrr->invalid country code, so we keep only the first part of inputVal
        if (inputVal.split(",")[1].length > 2) {
          inputVal = inputVal.split(",")[0];
          content = el
            .querySelector(".city-name span")
            .textContent.toLowerCase();
        } else {
          content = el.querySelector(".city-name").dataset.name.toLowerCase();
        }
      } else {
        //athens
        content = el.querySelector(".city-name span").textContent.toLowerCase();
      }
      return content == inputVal.toLowerCase();
    });

    if (filteredArray.length > 0) {
      msg.textContent = `You already know the weather for ${
        filteredArray[0].querySelector(".city-name span").textContent
      } ...otherwise be more specific by providing the country code as well`;
      form.reset();
      input.focus();
      return;
    }
  }


  //ajax here
  const url = `https://api.openweathermap.org/data/2.5/weather?q=${inputVal}&appid=${apiKey}&units=metric`;

  fetch(url)
    .then(response => response.json())
    .then(data => {
      const { main, name, sys, weather } = data;
      const icon = `https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${
        weather[0]["icon"]
      }.svg`;

      const li = document.createElement("li");
      li.classList.add("city");
      const markup = `
        <h2 class="city-name" data-name="${name},${sys.country}">
          <span>${name}</span>
          <sup>${sys.country}</sup>
        </h2>
        <div class="city-temp">${Math.round(main.temp)}<sup>°C</sup></div>
        <figure>
          <img class="city-icon" src="${icon}" alt="${
        weather[0]["description"]
      }">
          <figcaption>${weather[0]["description"]}</figcaption>
        </figure>
      `;
      li.innerHTML = markup;
      list.appendChild(li);
    })
    .catch(() => {
      msg.textContent = "Please search for a valid city";
    });

  msg.textContent = "";
  form.reset();
  input.focus();
}); */

// Music

let now_playing = document.querySelector(".now-playing");
let track_art = document.querySelector(".track-art");
let track_name = document.querySelector(".track-name");
let track_artist = document.querySelector(".track-artist");

let playpause_btn = document.querySelector(".playpause-track");

let seek_slider = document.querySelector(".seek_slider");
let volume_slider = document.querySelector(".volume_slider");
let curr_time = document.querySelector(".current-time");
let total_duration = document.querySelector(".total-duration");

let track_index = 0;
let isPlaying = false;
let updateTimer;

// Create new audio element
let curr_track = document.createElement('audio');

// Define the tracks that have to be played
let track_list = [
  {
    name: "ULTIMATE HEIGHTS",
    artist: "Total Recall",
    image: "https://images.pexels.com/photos/2264753/pexels-photo-2264753.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=250&w=250",
    path: "https://od.lk/s/MjZfNDM0NDY4NTVf/_DANCEMANIA__ULTIMATE_HEIGHTS_-_TOTAL_RECALL.mp3"
  },
  {
    name: "Enthusiast",
    artist: "Tours",
    image: "https://images.pexels.com/photos/3100835/pexels-photo-3100835.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=250&w=250",
    path: "https://hearthis.app/fluxxmusic/famie-joxx-forest-time-2022/download/?secret=w6brR"
  },
  {
    name: "Shipping Lanes",
    artist: "Chad Crouch",
    image: "https://images.pexels.com/photos/1717969/pexels-photo-1717969.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=250&w=250",
    path: "https://files.freemusicarchive.org/storage-freemusicarchive-org/music/ccCommunity/Chad_Crouch/Arps/Chad_Crouch_-_Shipping_Lanes.mp3",
  },
];



function loadTrack(track_index) {
  clearInterval(updateTimer);
  resetValues();
  curr_track.src = track_list[track_index].path;
  curr_track.load();

  track_art.style.backgroundImage = "url(" + track_list[track_index].image + ")";
  track_name.textContent = track_list[track_index].name;
  track_artist.textContent = track_list[track_index].artist;
 // now_playing.textContent = "PLAYING " + (track_index + 1) + " OF " + track_list.length;

  updateTimer = setInterval(seekUpdate, 1000);
}

function resetValues() {
  curr_time.textContent = "00:00";
  total_duration.textContent = "00:00";
  seek_slider.value = 0;
}

// Load the first track in the tracklist
loadTrack(track_index);

function playpauseTrack() {
  if (!isPlaying) playTrack();
  else pauseTrack();
}

function playTrack() {
  curr_track.play();
  isPlaying = true;
  playpause_btn.innerHTML = '<i class="fa fa-pause-circle fa-5x"></i>';
}

function pauseTrack() {
  curr_track.pause();
  isPlaying = false;
  playpause_btn.innerHTML = '<i class="fa fa-play-circle fa-5x"></i>';;
}

function seekTo() {
  let seekto = curr_track.duration * (seek_slider.value / 100);
  curr_track.currentTime = seekto;
}

function setVolume() {
  curr_track.volume = volume_slider.value / 100;
}

function seekUpdate() {
  let seekPosition = 0;

  if (!isNaN(curr_track.duration)) {
    seekPosition = curr_track.currentTime * (100 / curr_track.duration);

    seek_slider.value = seekPosition;

    let currentMinutes = Math.floor(curr_track.currentTime / 60);
    let currentSeconds = Math.floor(curr_track.currentTime - currentMinutes * 60);
    let durationMinutes = Math.floor(curr_track.duration / 60);
    let durationSeconds = Math.floor(curr_track.duration - durationMinutes * 60);

    if (currentSeconds < 10) { currentSeconds = "0" + currentSeconds; }
    if (durationSeconds < 10) { durationSeconds = "0" + durationSeconds; }
    if (currentMinutes < 10) { currentMinutes = "0" + currentMinutes; }
    if (durationMinutes < 10) { durationMinutes = "0" + durationMinutes; }

    curr_time.textContent = currentMinutes + ":" + currentSeconds;
    total_duration.textContent = durationMinutes + ":" + durationSeconds;
  }
}

let r = 0;
let b = 0;
let g = 0;

console.log("hour is" + hour);


/*switch(hour){
  case 0.9:
    r = 0;
    b = 0;
    g = 0;
    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
    changeColour();
    break;
  case 2:
    r = 51;
    g = 0;
    b = 21;
    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
    changeColour();
  case 10:
    r = 25;
    g = 0;
    b = 51;
    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
    changeColour();
    console.log("hello")
  case 4:
    r = 0;
    g = 51;
    b = 51;
    changeColour();
  case 5: 
    r = 208;
    g = 134;
    b = 24;
    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
    changeColour();
  case 6:
    r = 208;
    g = 208;
    b = 24;
    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
    changeColour();
  case 22:
    r = 102;
    g = 0;
    b = 102;
    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
    changeColour();

}*/


function changeColour(){
  function bgColour () {
          r = r + 4
          b = b + 4
          g = g + 4
          document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
          console.log("colour change");
  }

  setInterval(function () { bgColour(); }, 60000); // changes bg colour every min
} 


